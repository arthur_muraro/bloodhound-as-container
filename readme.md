# Bloodhound-as-container

neo4j 4.x requires openjdk 11, which is pretty pain in the ass to install on recent systems. So we make a neo4J container instead.

- Database : `bolt://localhost:7687`
- Username : `neo4j`
- Password : `bloodhound`

# Install :

You need sudo rights.

```bash
git clone https://gitlab.com/arthur_muraro/bloodhound-as-container.git
bash ./bloodhound-as-container/install.sh
```

Then, simply run `bloodhound`
