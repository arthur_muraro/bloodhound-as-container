#!/bin/bash
mkdir -p ~/exploits/windows/bloodhound/
cd ~/exploits/windows/bloodhound/
wget https://github.com/BloodHoundAD/BloodHound/releases/download/v4.3.1/BloodHound-linux-x64.zip
unzip BloodHound-linux-x64.zip
chmod +x BloodHound-linux-x64/BloodHound
chmod +x run_bloodhound.sh
sudo ln -s ~/exploits/windows/bloodhound/run_bloodhound.sh /usr/bin/bloodhound