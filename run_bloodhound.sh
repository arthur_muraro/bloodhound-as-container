#!/bin/bash
docker container stop neo4j
docker container rm neo4j
docker run -d --name neo4j -e NEO4J_AUTH=neo4j/bloodhound -p 7474:7474 -p 7687:7687 -v ~/exploits/windows/bloodhound/neo4j_db:/data neo4j
~/exploits/windows/bloodhound/BloodHound-linux-x64/BloodHound &